import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ExamComponent } from './exam/exam.component';
import { ResultDetailsComponent } from './result-details/result-details.component';

const routes: Routes = [{
  path: 'exam',
  component: ExamComponent
}, {
  path: 'result-details',
  component: ResultDetailsComponent
}, {
  path: '**',
  redirectTo: 'exam'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
