import { Injectable } from '@angular/core';
import { Observable, of, Subject } from 'rxjs';

@Injectable()
export class CountdownTimerService {
  private subject = new Subject<any>();
  
  constructor() { }

  getCounter() {
    return of(5);
  }

  onMessage(): Observable<any> {
    return this.subject.asObservable();
  }

  sendMessage(message: string) {
    this.subject.next({ text: message });
  }
}
