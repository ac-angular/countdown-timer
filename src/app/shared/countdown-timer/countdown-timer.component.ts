import { Component, EventEmitter, Injectable, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-countdown-timer',
  templateUrl: './countdown-timer.component.html',
  styleUrls: ['./countdown-timer.component.scss']
})
export class CountdownTimerComponent implements OnInit {

  @Input() countdownTimer: number;
  @Output() finishReason = new EventEmitter<string>();
  countDown: Subscription;
  defaultCountDownTimer: number = 0;
  initialDelay: number = 1000;
  isActive: boolean = false;
  subscription: Subscription;
  tick: number = 1000;

  constructor(
    private countdownTimerService: CountdownTimerService
  ) { }

  ngOnInit(): void {
    if (this.countdownTimer) {
      this.countDown = timer(1000, this.tick).subscribe(() => {
        --this.countdownTimer;
        if (this.countdownTimer === 0) {
          this.countDown.unsubscribe();
          this.finishReason.emit('complete');
        }
      });
      this.subscription = this.countdownTimerService.onMessage().subscribe(message => {
        if (message.text === 'stop') {
          this.countDown.unsubscribe();
          this.finishReason.emit('stop');
        }
      });
    } else {
      this.countdownTimer = this.defaultCountDownTimer;
    }
    this.isActive = true;
  }

}

import { Pipe, PipeTransform } from '@angular/core';
import { Subscription, timer } from 'rxjs';
import { CountdownTimerService } from './countdown-timer.service';

@Pipe({
  name: 'formatTime'
})

export class FormatTimePipe implements PipeTransform {
  transform(value: number): string {
    const hours: number = Math.floor(value / 3600);
    const minutes: number = Math.floor((value % 3600) / 60);
    let time = '';
    if (hours > 0) {
      time = ("00" + hours).slice(-2) + ":";
    }
    time += ("00" + minutes).slice(-2) + ":";
    time += ("00" + Math.floor(value - minutes * 60)).slice(-2);
    return time;
  }
}

@Injectable()
export class MyService {
  getCounter(tick) {
    return timer(0, tick);
  }
}