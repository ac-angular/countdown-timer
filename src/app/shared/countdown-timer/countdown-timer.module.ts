import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CountdownTimerComponent, FormatTimePipe } from './countdown-timer.component';
import {MatCardModule} from '@angular/material/card';
import { CountdownTimerService } from './countdown-timer.service';

@NgModule({
  declarations: [
    CountdownTimerComponent,
    FormatTimePipe
  ],
  imports: [
    CommonModule,
    MatCardModule
  ],
  exports: [
    CountdownTimerComponent,
    FormatTimePipe,
  ],
  providers: [
    CountdownTimerService
  ]
})
export class CountdownTimerModule { }
