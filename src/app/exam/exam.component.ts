import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NotificationsService } from '../services/notifications.service';
import { CountdownTimerService } from '../shared/countdown-timer/countdown-timer.service';

@Component({
  selector: 'app-exam',
  templateUrl: './exam.component.html',
  styleUrls: ['./exam.component.scss']
})
export class ExamComponent implements OnInit {
  title = 'countdown-timer';
  countdownTimer: number;
  navigateUrl: string = 'result-details';

  constructor(
    private router: Router,
    private notificationsService: NotificationsService,
    private countdownTimerService: CountdownTimerService
  ) { }

  ngOnInit(): void {
    this.getCounter();
  }
  
  countDownEnds(result: string) {
    let message = '';
    switch (result) {
      case 'complete':
        message = 'La cuenta regresiva ha concluido';        
        break;
      case 'stop':
        message = 'Se ha detenido la cuenta regresiva';        
        break;
    }
    this.notificationsService.infoAutoCloseTopRight(message, 'Se cambia de ruta al finalizar el conteo');
    setTimeout(() => {
      this.router.navigateByUrl('result-details');      
    }, 3000);
  }

  getCounter() {
    this.countdownTimerService.getCounter().subscribe(response => {
      this.countdownTimer = response;
    },
    (error) => {
      this.notificationsService.errorAutoCloseCenter('Se produjo un error', 'No se pudo obtener el contador');
    });
  }

  stop() {
    this.countdownTimerService.sendMessage('stop');
  }
}
