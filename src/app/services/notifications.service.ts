import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import Swal, { SweetAlertIcon, SweetAlertPosition } from 'sweetalert2';

export interface Data {
  title: string;
  text: string;
  icon: string;
  confirmText: string;
  position: string;
  showConfirmButton: boolean;
  showCancelButton: boolean;
  timer: number;
}

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {
  cancelButtonColor: string = '#eee';
  confirmButtonColor: string = "#fb8c00";
  confirmText: string = "Aceptar";
  data: Data;
  position: string = 'center';
  timer: number = 3000;

  constructor() { }

  /*********************************************************************************************
   *                                        SUCCESS RETURN
   *********************************************************************************************/
   successTwoButtonTopRight(title: string, text: string, confirmText?: string) {
    this.data = {
      title: title,
      text: text,
      icon: 'sucess',
      confirmText:  confirmText ?? this.confirmText,
      showConfirmButton: true,
      showCancelButton: true,
      timer: null,
      position: 'top-end'
    };
    this.showNotificationPromise();
  }

  /*********************************************************************************************
   *                                        SUCCESS BUTTON
   *********************************************************************************************/
  successButtonTopRight(title: string, text: string, confirmText?: string) {
    this.data = {
      title: title,
      text: text,
      icon: 'sucess',
      confirmText:  confirmText ?? this.confirmText,
      showConfirmButton: true,
      showCancelButton: false,
      timer: null,
      position: 'top-end'
    };
    this.showNotification();
  }

  successButtonCenter(title: string, text: string, confirmText?: string) {
    this.data = {
      title: title,
      text: text,
      icon: 'success',
      confirmText:  confirmText ?? this.confirmText,
      showConfirmButton: true,
      showCancelButton: false,
      timer: null,
      position: 'center'
    };
    this.showNotification();
  }

  /*********************************************************************************************
   *                                        INFO BUTTON
   *********************************************************************************************/
  infoButtonTopRight(title: string, text: string, confirmText?: string) {
    this.data = {
      title: title,
      text: text,
      icon: 'info',
      confirmText:  confirmText ?? this.confirmText,
      showConfirmButton: true,
      showCancelButton: false,
      timer: null,
      position: 'top-end'
    };
    this.showNotification();
  }
  
  infoButtonCenter(title: string, text: string, confirmText?: string) {
    this.data = {
      title: title,
      text: text,
      icon: 'info',
      confirmText:  confirmText ?? this.confirmText,
      showConfirmButton: true,
      showCancelButton: false,
      timer: null,
      position: 'center'
    };
    this.showNotification();
  }

  /*********************************************************************************************
   *                                        WARNING BUTTON
   *********************************************************************************************/
  warningButtonTopRight(title: string, text: string, confirmText?: string) {
    this.data = {
      title: title,
      text: text,
      icon: 'warning',
      confirmText:  confirmText ?? this.confirmText,
      showConfirmButton: true,
      showCancelButton: false,
      timer: null,
      position: 'top-end'
    };
    this.showNotification();
  }

  warningButtonCenter(title: string, text: string, confirmText?: string) {
    this.data = {
      title: title,
      text: text,
      icon: 'warning',
      confirmText:  confirmText ?? this.confirmText,
      showConfirmButton: true,
      showCancelButton: false,
      timer: null,
      position: 'center'
    };
    this.showNotification();
  }

  /*********************************************************************************************
   *                                        ERROR BUTTON
   *********************************************************************************************/
  errorButtonTopRight(title: string, text: string, confirmText?: string) {
    this.data = {
      title: title,
      text: text,
      icon: 'error',
      confirmText: confirmText ?? this.confirmText,
      showConfirmButton: true,
      showCancelButton: false,
      timer: null,
      position: 'top-end'
    };
    this.showNotification();
  }

  errorButtonCenter(title: string, text: string, confirmText?: string) {
    this.data = {
      title: title,
      text: text,
      icon: 'error',
      confirmText: confirmText ?? this.confirmText,
      showConfirmButton: true,
      showCancelButton: false,
      timer: null,
      position: 'center'
    };
    this.showNotification();
  }

  /*********************************************************************************************
   *                                        SUCCESS TIMER
   *********************************************************************************************/
  successAutoCloseTopRight(title: string, text: string, timer?: number) {
    this.data = {
      title: title,
      text: text,
      icon: 'success',
      confirmText: '',
      showConfirmButton: false,
      showCancelButton: false,
      timer: timer ?? this.timer,
      position: 'top-end'
    };
    this.showNotification();
  }

  successAutoCloseCenter(title: string, text: string, timer?: number) {
    this.data = {
      title: title,
      text: text,
      icon: 'success',
      confirmText: '',
      showConfirmButton: false,
      showCancelButton: false,
      timer: timer ?? this.timer,
      position: 'center'
    };
    this.showNotification();
  }

  /*********************************************************************************************
   *                                        INFO TIMER
   *********************************************************************************************/
   infoAutoCloseTopRight(title: string, text: string, timer?: number) {
    this.data = {
      title: title,
      text: text,
      icon: 'info',
      confirmText: '',
      showConfirmButton: false,
      showCancelButton: false,
      timer: timer ?? this.timer,
      position: 'top-end'
    };
    this.showNotification();
  }

  infoAutoCloseCenter(title: string, text: string, timer?: number) {
    this.data = {
      title: title,
      text: text,
      icon: 'info',
      confirmText: '',
      showConfirmButton: false,
      showCancelButton: false,
      timer: timer ?? this.timer,
      position: 'center'
    };
    this.showNotification();
  }

  /*********************************************************************************************
   *                                        WARNING TIMER
   *********************************************************************************************/
  warningAutoCloseTopRight(title: string, text: string, timer?: number) {
    this.data = {
      title: title,
      text: text,
      icon: 'warning',
      confirmText: '',
      showConfirmButton: false,
      showCancelButton: false,
      timer: timer ?? this.timer,
      position: 'top-end'
    };
    this.showNotification();
  }

  warningAutoCloseCenter(title: string, text: string, timer?: number) {
    this.data = {
      title: title,
      text: text,
      icon: 'warning',
      confirmText: '',
      showConfirmButton: false,
      showCancelButton: false,
      timer: timer ?? this.timer,
      position: 'center'
    };
    this.showNotification();
  }

  /*********************************************************************************************
   *                                        WARNING TIMER
   *********************************************************************************************/
   errorAutoCloseTopRight(title: string, text: string, timer?: number) {
    this.data = {
      title: title,
      text: text,
      icon: 'error',
      confirmText: '',
      showConfirmButton: false,
      showCancelButton: false,
      timer: timer ?? this.timer,
      position: 'top-end'
    };
    this.showNotification();
  }

  errorAutoCloseCenter(title: string, text: string, timer?: number) {
    this.data = {
      title: title,
      text: text,
      icon: 'error',
      confirmText: '',
      showConfirmButton: false,
      showCancelButton: false,
      timer: timer ?? this.timer,
      position: 'center'
    };
    this.showNotification();
  }

  /*********************************************************************************************
   *                                        NOTIFICATION
   *********************************************************************************************/
  private showNotification() {
    Swal.fire({
      title: this.data.title,
      text: this.data.text,
      icon: this.data.icon as SweetAlertIcon,
      confirmButtonColor: this.confirmButtonColor,
      confirmButtonText: this.data.confirmText,
      showConfirmButton: this.data.showConfirmButton,
      position: this.data.position as SweetAlertPosition,
      timer: this.data.timer
    });
  }

  private showNotificationPromise() {
    Swal.fire({
      title: this.data.title,
      text: this.data.text,
      icon: this.data.icon as SweetAlertIcon,
      confirmButtonText: this.data.confirmText,
      confirmButtonColor: this.confirmButtonColor,
      showConfirmButton: this.data.showConfirmButton,
      showCancelButton: true,
      cancelButtonColor: this.cancelButtonColor,
    }).then((result) => {
      return new Promise((resolve, reject) => {
        resolve(result);
      })
    });
  }
}